package com.delta.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;

public class Utils {
	private final static Logger _logger = LoggerFactory.getLogger(Utils.class);

	/**
	 * This method used for reading data from CSV file
	 * 
	 * @param fileReader
	 *            of the testData file
	 * @param offset
	 *            of the testData
	 * @return List of type String, String
	 **/

	@SuppressWarnings({ "resource", "unused" })
	public static List<Map<String, String>> readToMap(Reader fileReader,
			String offset) throws IOException {
		_logger.debug("Reading data from CSV file and return list of map");
		List<String> customerList;
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		ICsvListReader listReader = null;
		listReader = new CsvListReader(fileReader,
				CsvPreference.STANDARD_PREFERENCE);
		listReader.getHeader(true);
		while ((customerList = listReader.read()) != null) {
			String apiHeader = customerList.get(0);
			Map<String, String> map = new HashMap<String, String>();
			if (apiHeader.equals(offset)) {
				System.out.println(customerList.remove(0)
						+ " is the removed String");
				for (Iterator<String> listIt = customerList.iterator(); listIt
						.hasNext();) {
					String temp = listIt.next();
					if (!StringUtils.isBlank(temp)) {
						String[] intList = temp.split(":", 2);
						try {
							map.put(intList[0].trim(), intList[1]);
						} catch (ArrayIndexOutOfBoundsException e) {
							map.put(intList[0].trim(), "");
						}
					}
				}
				list.add(map);
			}
		}
		if (listReader == null) {
			throw new IOException(String.format(
					"Unable to read CSV with offset %s to a map", offset));
		}
		return list;
	}
	
	public static Reader stringToReader(String string) {
		_logger.debug("Return string data Reader");
		ClassLoader classLoader = CommonDataProvider.class.getClassLoader();
		InputStream stream = classLoader.getResourceAsStream(string);
		Reader targetReader = new InputStreamReader(stream);
		return targetReader;
	}
	
	/**
	 * This method used for reading data from CSV file
	 *
	 * @param fileReader of the testData file
	 * @param offset of the testData
	 * @return Map of type String,String
	 */
	@SuppressWarnings("resource")
	public static Map<String, String> readCSVToMap(Reader fileReader, String offset) throws IOException {
		_logger.debug("Returns Reader data to Map");
		Map<String, String> map = new HashMap<String, String>();
		List<String> customerList;
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		ICsvListReader listReader = null;
		listReader = new CsvListReader(fileReader, CsvPreference.STANDARD_PREFERENCE);
		listReader.getHeader(true);
		while ((customerList = listReader.read()) != null) {
			String apiHeader = customerList.get(0);
			 map = new HashMap<String, String>();
			if (apiHeader.equals(offset)) {
				//System.out.println(customerList.remove(0) + " is the removed String");
				for (Iterator<String> listIt = customerList.iterator(); listIt.hasNext(); ) {
					String temp = listIt.next();
					if (!StringUtils.isBlank(temp)) {
						String[] intList = temp.split(":", 2);
						try {
							map.put(intList[0].trim(), intList[1]);
						} catch (ArrayIndexOutOfBoundsException e) {
							map.put(intList[0].trim(), "");
						}
					}
				}
				return map;
			}
		}

		if (listReader == null) {
			throw new IOException(String.format(
					"Unable to read CSV with offset %s to a map",
					offset
			));
		}
		return map;
	}
}