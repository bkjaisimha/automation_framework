package com.delta.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;

public class CommonDataProvider {
	private final static Logger _logger = LoggerFactory.getLogger(CommonDataProvider.class);

	/**
	 * Generic DataProvider method to be used across all the test modules It
	 * takes the annotation class instance of the calling test method and gets
	 * the input csv file Parses the csv file for key value pairs and invokes
	 * the test method with the parsed params
	 * 
	 * @param testMethod
	 * @return
	 */
	@DataProvider(name = "testDataProvider")
	public static Object[][] UserCreateData(Method testMethod) throws IOException {
		String relFilePath = testMethod
				.getAnnotation(DataProviderArguments.class)
				.value()[0]
				.split("=")[1];
		String offset = testMethod
				.getAnnotation(DataProviderArguments.class)
				.value()[1]
				.split("=")[1];
		ClassLoader classLoader = CommonDataProvider.class.getClassLoader();
		String absCSVFile = classLoader.getResource(relFilePath).getFile();
		InputStream csvStream = classLoader.getResourceAsStream(relFilePath);
		Reader reader = new InputStreamReader(csvStream);
		_logger.debug("reading data from " + absCSVFile);
		List<Map<String, String>> lineList = Utils.readToMap(reader, offset);
		Object[][] result = new Object[lineList.size()][];
		int i = 0;
		for (Map<String, String> s : lineList) {
			result[i] = new Object[] { s };
			i++;
		}
		return result;
	}
}