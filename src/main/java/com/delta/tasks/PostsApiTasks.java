package com.delta.tasks;

import io.restassured.response.Response;

import java.io.IOException;
import java.util.Map;

import com.delta.utils.Utils;

public class PostsApiTasks {

	Map<String, String> testdata;

	public boolean postsResponse(Response response, String expectedResult)
			throws IOException {

		testdata = Utils
				.readCSVToMap(
						Utils.stringToReader("com/delta/testdata/poststestdata/posts_testdata.csv"),
						expectedResult);
		testdata.remove(expectedResult);

		if (response.body().equals(null)) {
			return false;
		}
		for (Map.Entry<String, String> result : testdata.entrySet()) {
			if (testdata.get(result.getKey()).equals(
					response.jsonPath().get(result.getKey()).toString())) {

			} else {
				return false;
			}
		}
		return true;
	}
}
