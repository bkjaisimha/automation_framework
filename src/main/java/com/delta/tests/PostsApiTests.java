package com.delta.tests;

import static io.restassured.RestAssured.given;
import io.restassured.response.Response;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.delta.core.TestBase;
import com.delta.reporting.ExecutionListener;
import com.delta.tasks.PostsApiTasks;
import com.delta.utils.CommonDataProvider;
import com.delta.utils.DataProviderArguments;

@Listeners(ExecutionListener.class)
public class PostsApiTests extends TestBase{

	private final static Logger _logger = LoggerFactory.getLogger(PostsApiTests.class);
	public PostsApiTasks postApitasks = new PostsApiTasks();
	@Test(description = "This test is to verify single comment body", 
			  dataProvider = "testDataProvider", 
			  dataProviderClass = CommonDataProvider.class, 
			  enabled = true
		)
		@DataProviderArguments({ "filePath=com/delta/testdata/poststestdata/posts_testdata.csv",
				                 "offsetName=posts_1_testData" })
		public void verifyIndivdualPostsApi(Map<String, String> testData) {
			Response response = null;
			try {
				_logger.info("Executing GET call");
				response = given()
	                    .with()
	                    .contentType("application/json")
	                    .expect()
	                    .statusCode(Integer.parseInt(testData.get("responseStatusCode")))
	                    .when().get(baseProperties.BASEURI+testData.get("apiUri"));
				Assert.assertEquals(response.statusCode(),
						Integer.parseInt(testData.get("responseStatusCode")),
						"response code for GET call is not as expected");
				_logger.info("Response code recieved as expected");
				Assert.assertTrue(postApitasks.postsResponse(response,testData.get("result")),"Response body is not as expected");
				_logger.info("Response body recieved as expected");
			} catch (Exception e) {
				_logger.error(e.getMessage());
			}
		}
	@Test(description = "This test is to verify comments api", 
		  dataProvider = "testDataProvider", 
		  dataProviderClass = CommonDataProvider.class, 
		  enabled = true
	)
	@DataProviderArguments({ "filePath=com/delta/testdata/poststestdata/posts_testdata.csv",
			                 "offsetName=posts_get_testData" })
	public void verifyPostsApi(Map<String, String> testData) {
		Response response = null;
		try {
			_logger.info("Executing GET call");
			response = given()
                    .with()
                    .contentType("application/json")
                    .expect()
                    .statusCode(Integer.parseInt(testData.get("responseStatusCode")))
                    .when().get(baseProperties.BASEURI+testData.get("apiUri"));
			Assert.assertEquals(response.statusCode(),
					Integer.parseInt(testData.get("responseStatusCode")),
					"response code for GET call is not as expected");
			_logger.info("Response code recieved as expected");
		} catch (Exception e) {
			_logger.error(e.getMessage());
			Assert.fail();
		}
	}
}
