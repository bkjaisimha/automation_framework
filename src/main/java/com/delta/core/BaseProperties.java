package com.delta.core;

import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

/**
 * Main properties file for Automation Framework
 *
 * @Configuration - tell spring that this is also a config file
 * @PropertySource - tell spring that this class acts as a source of property
 */
@Configuration
@PropertySources({
		@PropertySource("classpath:BaseProperties.properties"),
		@PropertySource(value = "${BaseProperties.properties}", ignoreResourceNotFound = true)
})
public class BaseProperties {

	public static final BaseProperties baseProperties;
	private static final Logger LOGGER = LoggerFactory.getLogger(BaseProperties.class);

	/**
	 * Creates the instance of the class
	 */
	static {
		org.apache.log4j.Logger.getLogger("org.springframework").setLevel(Level.WARN);
		org.apache.log4j.Logger.getLogger("org").setLevel(Level.WARN);
		LOGGER.debug("baseProperties, #static() called");
		baseProperties = new AnnotationConfigApplicationContext(BaseProperties.class).getBean(BaseProperties.class);
	}

	/**
	 * Returns the singleton of the spring properties
	 *
	 * @return
	 */
	public static BaseProperties get() {
		return baseProperties;
	}

	@Value("${host.ip}")
	public String HOST_IP;

	@Value("${host.username}")
	public String HOST_USERNAME;

	@Value("${host.password}")
	public String HOST_PASSWORD;

	@Value("${baseuri}")
	public String BASEURI;


}
