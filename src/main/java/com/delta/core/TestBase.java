package com.delta.core;

import io.restassured.RestAssured;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestBase {
	private static final Logger _logger = LoggerFactory.getLogger(TestBase.class);
	public final BaseProperties baseProperties = BaseProperties.get();
	
	/** 
	 * constructor to initialize base functions common for all the tests
	 */
	public TestBase() {
		_logger.info("Initializing test base");
        RestAssured.useRelaxedHTTPSValidation();
	}
}
