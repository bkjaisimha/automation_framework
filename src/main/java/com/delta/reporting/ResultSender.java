package com.delta.reporting;

import static org.apache.commons.math3.util.Precision.round;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ResultSender {
    private static final Logger _logger = LoggerFactory.getLogger(ResultSender.class);

	public static void generateHTML(){
        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jsonObject = jsonParser.parse(new FileReader("test-output/testngJsonData.json")).getAsJsonObject();

            JsonObject dataObject = jsonObject.get("Data").getAsJsonArray().get(0).getAsJsonObject();
            JsonArray passedArray = dataObject.get("passed").getAsJsonArray();
            JsonArray failedArray = dataObject.get("failed").getAsJsonArray();
            JsonArray skippedArray = dataObject.get("skipped").getAsJsonArray();
            String suiteStartTime = dataObject.get("testStartTime").getAsString();
            long time = Long.parseLong(dataObject.get("suiteTime").getAsString().replace(".",""));
            String suiteExecTime = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(time),
                    TimeUnit.MILLISECONDS.toMinutes(time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(time)),
                    TimeUnit.MILLISECONDS.toSeconds(time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(time)));
            float pass=0.0f, fail=0.0f, skip=0.0f;
            HashSet<String> groupsSet = new HashSet<>();
            StringBuilder failedTableData = new StringBuilder();
            StringBuilder skippedTableData = new StringBuilder();

            if(passedArray.size() != 0) {
                pass = passedArray.get(passedArray.size() - 1).getAsJsonObject().get("numberOfTestCases").getAsFloat();
                if(failedArray.size() != 0) {
                fail = failedArray.get(failedArray.size() - 1).getAsJsonObject().get("numberOfTestCases").getAsFloat();
                for (int i=0; i<failedArray.size()-1; i++) {
                    failedTableData.append("<tr><td>" + failedArray.get(i).getAsJsonObject().get("name").toString()
                            + "</td><td>" + failedArray.get(i).getAsJsonObject().get("description").toString()
                            + "</td><td>" + failedArray.get(i).getAsJsonObject().get("exception").toString() + "</td></tr>");
                }
            }
            if(skippedArray.size() != 0) {
                skip = skippedArray.get(skippedArray.size() - 1).getAsJsonObject().get("numberOfTestCases").getAsFloat();
                for (int i=0; i<skippedArray.size()-1; i++) {
                    skippedTableData.append("<tr><td>"+skippedArray.get(i).getAsJsonObject().get("name").getAsString()
                            +"</td><td>"+skippedArray.get(i).getAsJsonObject().get("description").getAsString()
                            +"</td><td>"+skippedArray.get(i).getAsJsonObject().get("exception").getAsString()+"</td></tr>");
               }
            }
            float passPercent = round((pass*100)/(pass+fail+skip), 2);
            float failPercent = round((fail*100)/(pass+fail+skip), 2);
            float skipPercent = round((skip*100)/(pass+fail+skip), 2);
            InputStream in = ResultSender.class.getResourceAsStream("/report-template.html"); 
            String htmlString = IOUtils.toString(in);
            
            htmlString = htmlString.replace("$suiteStartTime", suiteStartTime);
            htmlString = htmlString.replace("$pass", String.valueOf(passPercent));
            htmlString = htmlString.replace("$remainingPass", String.valueOf(round(100-passPercent,2)));
            htmlString = htmlString.replace("$fail", String.valueOf(failPercent));
            htmlString = htmlString.replace("$remainingFail", String.valueOf(round(100-failPercent,2)));
            htmlString = htmlString.replace("$skip", String.valueOf(skipPercent));
            htmlString = htmlString.replace("$remainingSkip", String.valueOf(round(100-skipPercent,2)));
            htmlString = htmlString.replace("$countPassedTestCase", String.valueOf((int)pass));
            htmlString = htmlString.replace("$countFailedTestCaseCount", String.valueOf((int)fail));
            htmlString = htmlString.replace("$countSkippedTestCaseCount", String.valueOf((int)skip));
            htmlString = htmlString.replace("$testSuiteTime", suiteExecTime);
            htmlString = htmlString.replace("$dataFailed", failedTableData);
            htmlString = htmlString.replace("$dataSkipped", skippedTableData);
            File newHtmlFile = new File("test-output/custom_report.html");
            FileUtils.writeStringToFile(newHtmlFile, htmlString, "UTF-8");
            }} catch (IOException e ) {
        	e.printStackTrace();
            _logger.debug(e.getLocalizedMessage());
        }catch (NullPointerException e ) {
        	e.printStackTrace();
            _logger.debug(e.getLocalizedMessage());
            return;
        } 
    }

    public static void writeDataToFile(JSONObject jsonObject){
        try {
            FileWriter file = new FileWriter("test-output/testngJsonData.json");
            file.write(jsonObject.toJSONString());
            file.flush();
            file.close();
        } catch (IOException e) {
            _logger.debug(e.getLocalizedMessage());
        }
    }
}
