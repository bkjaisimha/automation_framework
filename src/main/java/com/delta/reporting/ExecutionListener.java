package com.delta.reporting;


import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class ExecutionListener implements IReporter  {

    @SuppressWarnings("unchecked")
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
        JSONArray results = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        suites.forEach(element->{
            results.add(createSuiteJsonObject(element));
        });
        jsonObject.put("Data", results);
        ResultSender.writeDataToFile(jsonObject);
        ResultSender.generateHTML();
    }

    @SuppressWarnings("unchecked")
    public JSONObject createSuiteJsonObject(ISuite suite) {
        JSONObject result = new JSONObject();
        JSONArray passedMethods = new JSONArray();
        JSONArray failedMethods = new JSONArray();
        JSONArray skippedMethods = new JSONArray();
        suite.getResults().values().forEach(element -> {
            ITestContext context = element.getTestContext();
            passedMethods.addAll(createResultJsonArray(context.getPassedTests().getAllResults()));
            failedMethods.addAll(createResultJsonArray(context.getFailedTests().getAllResults()));
            skippedMethods.addAll(createResultJsonArray(context.getSkippedTests().getAllResults()));
        });

        Date startDate = suite.getResults().values().stream().map(s -> s.getTestContext().getStartDate())
                .min(Date::compareTo).get();
        long startingTime = startDate.getTime();
        long endingTime = suite.getResults().values().stream().map(s -> s.getTestContext().getEndDate())
                .max(Date::compareTo).get().getTime();
        result.put("name", suite.getName());
        result.put("testStartTime", startDate.toString());
        result.put("suiteTime", ((endingTime-startingTime)/1000.0));
        result.put("passed", passedMethods);
        result.put("failed", failedMethods);
        result.put("skipped", skippedMethods);
        return result;
    }

    @SuppressWarnings("unchecked")
    public JSONArray createResultJsonArray(Set<ITestResult> results) {
        JSONArray result = new JSONArray();
        JSONObject additionalDetails = new JSONObject();
        results.forEach(element ->{
            JSONObject currentJsonResult = new JSONObject();
            currentJsonResult.put("name", element.getName());
            currentJsonResult.put("testCaseTime", ((element.getEndMillis() - element.getStartMillis())/1000.0));
            if(!element.isSuccess())
                if(String.valueOf(element.getThrowable()).equalsIgnoreCase("null"))
                    currentJsonResult.put("exception", "Test case did not throw exception");
                else
                    currentJsonResult.put("exception", element.getThrowable().getLocalizedMessage());
            currentJsonResult.put("description", String.valueOf(element.getMethod().getDescription()));
            result.add(currentJsonResult);
        });
        additionalDetails.put("numberOfTestCases",result.size());
        result.add(additionalDetails);
        return result;
    }
}