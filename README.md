# Automation_framework

## Name
API Automation Framework using RestAssured and Custom Reporting using TestNG.

## Description
This project cotains API test framework, which i have developed using RestAssured and TestNG. 
I have used mock api to perform GET action on _/posts_ api 


## Getting started

To clone this repo and build.

git clone repo
cd automation_framework

**Run the following command to build the project**- 
 
 mvn clean test

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:f689bb5fae05b775b97866165a2b56ef?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:f689bb5fae05b775b97866165a2b56ef?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:f689bb5fae05b775b97866165a2b56ef?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/bkjaisimha/automation_framework.git
git branch -M main
git push -uf origin main
```

## Test and Deploy

**To run the tests, run the following in terminal:**

java -cp "path of class files; testng jar file path" org.testng.TestNG testng.xml
ex:-

java -cp ".\bin;..\common_lib\*;" org.testng.TestNG run_api_tests.xml

**TO Run Tests in IDE, execute run_api_tests.xml file**


## Authors and acknowledgment
The Mock API is provided by [JSONPlaceholder - Free Fake REST API](https://jsonplaceholder.typicode.com/).

